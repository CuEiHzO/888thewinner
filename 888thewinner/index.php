<!DOCTYPE html>
<html lang="en">

<head>

	<?php include('inc_head.php'); ?>
	<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
	<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>

<style>
	.box-out {
		background: #fff;
		min-height: 350px;
		width: 300px;
		position: fixed;
		bottom: 0px;
		left: 0px;
		margin: 0;
		-webkit-transition: .5s ease-out;
		-moz-transition: .5s ease-out;
		-o-transition: .5s ease-out;
		-ms-transition: .5s ease-out;
		transition: .5s ease-out;
	}

	.box-in {
		margin: 0 0 0 -300px;
	}

	.open_box {
		position: absolute;
		top: 0;
		left: 0;
		width: 350px;
		height: 45px;
		font-size: 20px;
		padding: 7px 10px;
		text-align: right;
		background-size: cover;
		cursor: pointer;
		background-color: #edcd00;
		color: #000;
	}

	.open_box i {
		width: 30px;
		font-size: 30px;
		margin-left: 10px;
	}

</style>

<body>
	<?php include('inc_header.php'); ?>
	<!-- content here-->
	<div class="wrapper">
		<div class="container-fluid c88_content">
			<div class="row">
				<div class="col-12 col-md-3">
					<!--
					<div class="input-group datepicker_table">
						<label>สร้างวันที่แข่ง</label>
						<div class="custom-file">
							<input id="datepicker" type="personal" class="form-control" />
						</div>
						<div class="input-group-append">
							<button class="send" type="button">สร้าง</button>
						</div>
					</div>
-->
				</div>
				<div class="col-12 col-md-5">
					<!--					<button class="manage-table"><i class="fas fa-table"></i> จัดการตารางแข่งขัน</button>-->
				</div>
				<div class="col-12 col-md-4">
					<div class="input-group tableMatch row">
						<label>แสดงตารางแข่ง</label>
						<select class="month-select form-control" id="month">
							<option selected>เดือน</option>
							<option value="1"></option>
							<option value="2"></option>
							<option value="3"></option>
						</select>
						<select class="year-select form-control" id="year">
							<option selected>ปี</option>
							<option value="1"></option>
							<option value="2"></option>
							<option value="3"></option>
						</select>
						<div class="input-group-append">
							<button class="send">แสดง</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-9">
					<div class="t88-table">
						<div class="table-responsive">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th align="center" rowspan="2" width="10%">เวลา</th>
										<th rowspan="2" width="30%">คู่แข่งขัน</th>
										<th colspan="3" width="30%">อัตราต่อรอง</th>
										<th colspan="3" width="30%">สูง-ต่ำ</th>
									</tr>
									<tr>
										<th>ราคา</th>
										<th>เจ้าบ้าน</th>
										<th>ทีมเยือน</th>
										<th>ราคา</th>
										<th>สูง</th>
										<th>ต่ำ</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="8" class="tournament">ยูฟ่า ยูโรป้า ลีก</td>
									</tr>
									<tr>
										<td>1-0 <br> HT</td>
										<td><span class="bet">บาเลนเซีย ซีเอฟ</span><br><span>อาร์เซนอล</span> </td>
										<td>0.5</td>
										<td>0.90</td>
										<td>-0.98</td>
										<td>0.93</td>
										<td>0.97</td>
										<td>0.97</td>
									</tr>
									<tr>
										<td>LIVE<br>02:00pm</td>
										<td><span class="bet">บาเลนเซีย ซีเอฟ</span><br><span>อาร์เซนอล</span> </td>
										<td>0.5</td>
										<td>0.90</td>
										<td>-0.98</td>
										<td>0.93</td>
										<td>0.97</td>
										<td>0.97</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-3">
					<div class="panel-t88">
						<h4 class="title-header">ใบพนัน</h4>
						<div class="scroll-bet">
							<div class="bet-t88">
								<p style="margin-bottom: 5px; color: #ed1c24;">ยูฟ่า ยูโรป้า ลีก</p>
								<div class="form-group form-check">
									<input type="checkbox" class="form-check-input" id="exampleCheck1">
									<label class="form-check-label" for="exampleCheck1">บาเลนเซีย ซีเอฟ กับ อาร์เซนอล</label>
									<button type="button" class="close" aria-hidden="true">×</button>
								</div>
								<form>
									<div class="form-group">
										<label for="bet">เงินเดิมพัน</label>
										<input type="bet" class="form-control" id="bet">
									</div>
									<div class="form-group">
										<label for="get-money">ได้ประมาณ</label>
										<input type="get-money" class="form-control" id="get-money">
									</div>
									<div class="form-group buttonBet">
										<button>แทง</button>
									</div>
								</form>
							</div>
							<div class="bet-t88">
								<p style="margin-bottom: 5px; color: #ed1c24;">ยูฟ่า ยูโรป้า ลีก</p>
								<div class="form-group form-check">
									<input type="checkbox" class="form-check-input" id="exampleCheck1">
									<label class="form-check-label" for="exampleCheck1">บาเลนเซีย ซีเอฟ กับ อาร์เซนอล</label>
									<button type="button" class="close" aria-hidden="true">×</button>
								</div>
								<form>
									<div class="form-group">
										<label for="bet">เงินเดิมพัน</label>
										<input type="bet" class="form-control" id="bet">
									</div>
									<div class="form-group">
										<label for="get-money">ได้ประมาณ</label>
										<input type="get-money" class="form-control" id="get-money">
									</div>
									<div class="form-group buttonBet">
										<button>แทง</button>
									</div>
								</form>
							</div>
							<div class="bet-t88">
								<p style="margin-bottom: 5px; color: #ed1c24;">ยูฟ่า ยูโรป้า ลีก</p>
								<div class="form-group form-check">
									<input type="checkbox" class="form-check-input" id="exampleCheck1">
									<label class="form-check-label" for="exampleCheck1">บาเลนเซีย ซีเอฟ กับ อาร์เซนอล</label>
									<button type="button" class="close" aria-hidden="true">×</button>
								</div>
								<form>
									<div class="form-group">
										<label for="bet">เงินเดิมพัน</label>
										<input type="bet" class="form-control" id="bet">
									</div>
									<div class="form-group">
										<label for="get-money">ได้ประมาณ</label>
										<input type="get-money" class="form-control" id="get-money">
									</div>
									<div class="form-group buttonBet">
										<button>แทง</button>
									</div>
								</form>
							</div>
							<div class="bet-t88">
								<p style="margin-bottom: 5px; color: #ed1c24;">ยูฟ่า ยูโรป้า ลีก</p>
								<div class="form-group form-check">
									<input type="checkbox" class="form-check-input" id="exampleCheck1">
									<label class="form-check-label" for="exampleCheck1">บาเลนเซีย ซีเอฟ กับ อาร์เซนอล</label>
									<button type="button" class="close" aria-hidden="true">×</button>
								</div>
								<form>
									<div class="form-group">
										<label for="bet">เงินเดิมพัน</label>
										<input type="bet" class="form-control" id="bet">
									</div>
									<div class="form-group">
										<label for="get-money">ได้ประมาณ</label>
										<input type="get-money" class="form-control" id="get-money">
									</div>
									<div class="form-group buttonBet">
										<button>แทง</button>
									</div>
								</form>
							</div>
							<div class="bet-t88">
								<p style="margin-bottom: 5px; color: #ed1c24;">ยูฟ่า ยูโรป้า ลีก</p>
								<div class="form-group form-check">
									<input type="checkbox" class="form-check-input" id="exampleCheck1">
									<label class="form-check-label" for="exampleCheck1">บาเลนเซีย ซีเอฟ กับ อาร์เซนอล</label>
									<button type="button" class="close" aria-hidden="true">×</button>
								</div>
								<form>
									<div class="form-group">
										<label for="bet">เงินเดิมพัน</label>
										<input type="bet" class="form-control" id="bet">
									</div>
									<div class="form-group">
										<label for="get-money">ได้ประมาณ</label>
										<input type="get-money" class="form-control" id="get-money">
									</div>
									<div class="form-group buttonBet">
										<button>แทง</button>
									</div>
								</form>
							</div>
						</div>
						<div class="total-t88">
							<h4 class="title-header">เงินเดิมพัน ต่อ พนัน </h4>
							<div class="total-bet">
								<input type="total-bet" class="form-control" id="total-bet" style="margin-bottom: 10px;">
								<p><b>รวม 2 พนัน : </b> EUR</p>
								<p><b>การจ่ายเงินโดยประมาณ : </b> EUR</p>
								<button>วางพนัน</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="box-out">
			<div class="open_box">
				<p>รายการใบพนัน <i class="far fa-futbol"></i></p>
			</div>

		</div>
	</div>
	<!-- end content here -->
	<?php include('inc_footer.php'); ?>

	<script>
		$('.open_box').click(function() {
			$(this).toggleClass("menu-btn-left");
			$('.box-out').toggleClass('box-in');
		});

	</script>

</body>

</html>
