<?php
	include('class/Mobile_Detect.php');
	$detect = new Mobile_Detect;
	$device = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'mobile') : 'desktop');
?>
<title>888thewinner</title>
<meta charset="utf-8">
<meta name="keywords" content="">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- open if have favicon
<?php include('favicon.php'); ?>
-->
<link type="text/css" rel="stylesheet" href="css/reset.css" media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/print.css" media="print" />

<link href="https://fonts.googleapis.com/css?family=Roboto:300,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Kanit:300,500,700" rel="stylesheet">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<link type="text/css" rel="stylesheet" href="css/layout.css" media="screen,projection" />



<script type="text/javascript" src="js/jquery-3.3.1.slim.min.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
