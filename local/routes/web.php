<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth'], function (){
	
	Route::group(['prefix' => 'admin' ,'middleware' => 'guard:Admin,admin/login'], function () {
		Route::resource('', 'Backend\DashboardController');
		
		Route::resource('league', 'Backend\LeagueController');
		Route::resource('match', 'Backend\MatchController');
		Route::resource('renew', 'Backend\RenewController');
		Route::resource('member', 'Backend\UserController');
		Route::resource('user', 'Backend\UserController');
		
		Route::post('league/datatable', 'Backend\LeagueController@Datatable');
		Route::post('league/status/{id}/{status}', 'Backend\LeagueController@Status');
		Route::post('match/datatable', 'Backend\MatchController@Datatable');
		Route::post('user/datatable', 'Backend\UserController@Datatable');
	});
	
	Route::group(['prefix' => 'member'], function () {
		Route::resource('', 'Member\DashboardController');
		Route::resource('deposit', 'Member\DepositController');
		Route::resource('withdrawal', 'Member\WithdrawalController');
		Route::resource('port', 'Member\PortController');
		Route::resource('rebalance', 'Member\RebalanceController');
		Route::resource('renew', 'Member\RenewController');
		Route::resource('user', 'Member\UserController');
	});
	
});

Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');


/*
Route::group(['prefix' => 'admin'], function () {
	Route::group(['middleware' => 'guard:admin,admin/login'],function(){
		Route::resource('', 'Backend\DashboardController');
	});
});
*/


Route::resource('/', 'HomeController');
Route::post('user/bet', 'HomeController@betMatche');
Route::get('user/bet/{id}', 'HomeController@betPopup');


//Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');
