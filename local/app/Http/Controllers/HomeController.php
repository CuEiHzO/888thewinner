<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$sRow 	= \App\Models\Matche::join('ck_league','ck_league.id','=','ck_matche.league_id')
				->select('ck_league.*', 'ck_matche.*', 'ck_matche.id as id')
				->where('league_status','Y')
				->whereDate('matche_date','>=',date('Y-m-d H:i:s'))
				->whereDate('matche_date','<',date('Y-m-d H:i:s',(time()+86400)))
				->orderBy('matche_date', 'asc')
				->orderBy('ck_matche.id', 'asc')
				->get();

		return view('frontend.home')->with(array('sRow'=> $sRow));
    }
	
    public function betPopup($id)
    {
		$sRow = \App\Models\Bet::find($id);
		return view('frontend.bet_print')->with(array('sRow'=> $sRow));
	}
	
	
    public function betMatche()
    {
		if( request('myBet') ){
			$sBet = \App\Models\Bet::whereDate('created_at', date('Y-m-d'))->orderBy('created_at','desc')->first();
			if( $sBet ){ $code = substr($sBet->code, -3)+1; }else{ $code = 1; }
			
			$sBet = new \App\Models\Bet;
			$sBet->amount	= 0;
			//$sBet->users_id	= \Auth::user()->id;
			$sBet->users_id	= 0;
			$sBet->code		= date('ymd').'-'.sprintf('%03d',$code);
			$sBet->save();
			
			foreach( request('myBet') AS $bet ){
				$sMatche = \App\Models\Matche::find($bet['id']);
				if( $sMatche ){
					if( $bet['type'] == 'home' ){ 	$bet1 = $sMatche->hdp_f; 		$bet2 = $sMatche->hdp_f_a; 	}
					if( $bet['type'] == 'away' ){ 	$bet1 = $sMatche->hdp_f; 		$bet2 = $sMatche->hdp_f_b; 	}
					if( $bet['type'] == 'over' ){ 	$bet1 = $sMatche->ou_f; 		$bet2 = $sMatche->ou_f_a; 		}
					if( $bet['type'] == 'under' ){ 	$bet1 = $sMatche->ou_f; 		$bet2 = $sMatche->ou_f_b; 		}

					$sRow = new \App\Models\BetList;
					$sRow->bet_id		= $sBet->id;
					$sRow->matche_id	= $bet['id'];
					$sRow->type 		= $bet['type'];
					$sRow->amount 		= $bet['amount'];
					$sRow->bet1 		= $bet1;
					$sRow->bet2 		= $bet2;
					$sRow->save();
					
					$sBet->amount += $bet['amount'];
				}
			}
			$sBet->save();
		}
		return response()->json(array('status'=>'success', 'code' => $sBet->id));	
    }
	
}
