<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiSoccerController extends Controller
{
	var $_url = 'https://api.totalcorner.com/v1/';
	var $_key = '29b0bd5cf2cb1915';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function upcoming()
    {
		set_time_limit(300);
		return $this->getMatche(1);
    }

	public function getOdds()
    {
		$this->odds('status-run');
		sleep(10);
		$this->odds('status-null');
		sleep(10);
		$this->odds('status-null');
    }
	
	public function odds($type)
    {
		if($type == 'status-null'){
			$sRow 	= \App\Models\Matche::whereNull('status')->limit(4)->orderBy('updated_at', 'asc')->get();
		}
		if($type == 'status-run'){
			$sRow 	= \App\Models\Matche::whereNotNull('status')->where('status','!=','full')->limit(4)->orderBy('updated_at', 'asc')->get();
		}
		if( $sRow ){
			foreach( $sRow  AS $r ){
					$uri 	= $this->_url.'match/odds/'.$r->id.'?token='.$this->_key.'&columns=asianList,goalList';
					$curl 	= new \App\Helpers\Curl('cookie');
					$sData 	= $curl->Get($uri);
					if( $sData ){
						echo $r->id;
						$row = json_decode($sData, true);
						if( isset($row) && $row['success'] == 1 ){
							echo ' : ';
							$sRow= \App\Models\Matche::find($r->id);
							if($sRow){
								$sRow->status				= $row['data'][0]['status'];
								$sRow->hf_hg				= $row['data'][0]['hf_hg'];
								$sRow->hf_ag				= $row['data'][0]['hf_ag'];
								if(count($row['data'][0]['asian_list'])){
									echo ' : ';
									echo 'asian_list';
									$sRow->handicap_handicap	= $row['data'][0]['asian_list'][0][1];
									$sRow->handicap_home		= $row['data'][0]['asian_list'][0][2];
									$sRow->handicap_away		= $row['data'][0]['asian_list'][0][3];
								}
								if(count($row['data'][0]['goal_list'])){
									echo ' : ';
									echo 'goal_list';
									$sRow->goals_goals			= $row['data'][0]['goal_list'][0][1];
									$sRow->goals_over			= $row['data'][0]['goal_list'][0][2];
									$sRow->goals_under			= $row['data'][0]['goal_list'][0][3];
								}
								$sRow->save();
								//return json_encode(array('status'=>'success'));
								echo '<br/>';
							}
						}
					}else{
						echo $r->id;
						echo ': <br/>';
					}
			}
		}
    }
	
	
    public function getMatche( $page=1 )
    {
		$uri 	= $this->_url.'match/today?token='.$this->_key.'&type=upcoming&columns=events,odds&page='.$page;
		$curl 	= new \App\Helpers\Curl('cookie');
		$sData 	= $curl->Get($uri);
		if( $sData ){
			$sData = json_decode($sData, true);
			if( isset($sData) && $sData['success'] == 1 ){
				if( isset($sData['data']) ){
					foreach( $sData['data'] AS $row ){
						$sRow= \App\Models\Matche::find($row['id']);
						if( empty($sRow) ){
							$sRow = new \App\Models\Matche;
							$sRow->id			= $row['id'];
							$sRow->h			= $row['h'];
							$sRow->h_id			= $row['h_id'];
							$sRow->a			= $row['a'];
							$sRow->a_id			= $row['a_id'];
							$sRow->l			= $row['l'];
							$sRow->l_id			= $row['l_id'];
						}
						$sRow->start			= $row['start'];
						$sRow->status			= $row['status'];
						$sRow->hf_hg			= $row['hf_hg'];
						$sRow->hf_ag			= $row['hf_ag'];
						$sRow->save();
						
						$sRow= \App\Models\League::find($row['l_id']);
						if( empty($sRow) ){
							$sRow= new \App\Models\League;
							$sRow->id = $row['l_id'];
							$sRow->name = $row['l'];
							$sRow->save();
						}
					}
				}
				
				if( $sData['pagination']['next'] == 'true' ){
					echo $page;
					$page++;
					return $this->getMatche($page);
				}
			}
		}
    }
}
