<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class MatchController extends Controller
{
    public function index()
    {
		$sLeague = \App\Models\League::search()->orderBy('league_name', 'asc')->get();
        return view('backend.match.index',['sLeague'=>$sLeague]);
    }
	
	
	public function Datatable(){
		$sTable = \App\Models\Matche::search()->orderBy('matche_start', 'asc');


		
		if( request('Custom') ){
			$sTable->whereDate('matche_date', '=', date('Y-m-d',strtotime(request('Custom.matche_date'))));
		}else{
			$sTable->whereDate('matche_date', '>=', date('Y-m-d'));
		}
		
		
		$sQuery	= DataTables::of($sTable)
		->addColumn('league_name',function($data){
			return empty($data->league_id)?'-':$data->league->league_name;
		})
		->addColumn('score',function($data){
			return !isset($data->score_home)?'-':$data->score_home.' - '.$data->score_away;
		})
		->editColumn('matche_start',function($data){
			return empty($data->matche_start)?'-':date('d-m-Y H:i', strtotime($data->matche_start));
		})
		->editColumn('updated_at',function($data){
			return empty($data->updated_at)?'-':date('d-m-Y H:i', strtotime($data->updated_at));
		})
		->editColumn('matche_status',function($data){
			if($data->matche_status=='0') return '<span class="label label-default">รอแข่งขัน</span>';
			if($data->matche_status=='1') return '<span class="label label-warning">กำลังแข่งขัน</span>';
			if($data->matche_status=='2') return '<span class="label label-success">จบครึ่งแรก</span>';
			if($data->matche_status=='3') return '<span class="label label-success">กำลังแข่งขัน</span>';
			if($data->matche_status=='4') return '<span class="label label-success">จบแข่งขัน</span>';
		})
		->escapeColumns(null);
		return $sQuery->make(true);
	}
	public function update($id){
		$sRow = \App\Models\Matche::find($id);
		$sRow->score_home = request('score_home');
		$sRow->score_away = request('score_away');
		$sRow->save();
		return response()->json(\App\Models\Alert::Msg('success'));
	}
}
