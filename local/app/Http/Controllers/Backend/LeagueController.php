<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class LeagueController extends Controller
{
    public function index()
    {
        return view('backend.league.index');
    }
	
	
	public function Datatable(){
		$sTable = \App\Models\League::search()->orderBy('league_name', 'asc');

		$sQuery	= DataTables::of($sTable);
		return $sQuery->make(true);
	}
	
	public function Status($id,$status){
		$sRow = \App\Models\League::find($id);
		$sRow->league_status=$status;
		$sRow->save();
		return json_encode(array('status'=>'success', 'msg'=>'บันทึกข้อมูลเรียบร้อย'));
	}
}
