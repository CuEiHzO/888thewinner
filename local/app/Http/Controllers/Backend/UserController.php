<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        return view('backend.user.index');
    }
	
    public function create()
    {
        return view('backend.user.form');
    }
	
    public function store(Request $request)
    {
		return $this->form();
    }
	
    public function update(Request $request, $id)
    {
		return $this->form($id);
    }
	
    public function form($id=NULL)
    {
		\DB::beginTransaction();
		try {
			if( $id ){
				$sData = \App\Models\User::find($id);
			}else{
				$sData = new \App\Models\User;
				$sData->username	= request('username');
			}
			$sData->first_name		= request('first_name');
			$sData->last_name		= request('last_name');
			$sData->phone			= request('phone');
			$sData->email			= request('email');
            if( request('password') ){
                $sData->password	= \Hash::make( request('password') );
            }
			$sData->save();
			\DB::commit();
		
			return redirect()->action('Backend\UserController@edit',$sData->id)->with(['alert'=>\App\Models\Alert::Msg('success')]);
		} catch (\Exception $e) {
			echo $e->getMessage();
			\DB::rollback();
			return redirect()->action('Backend\UserController@index')->with(['alert'=>\App\Models\Alert::e($e)]);
		}
	}
	
	
    public function edit($id)
    {
		/*try {*/
			$sRow 	= \App\Models\User::find($id);
			//$sUag	= new \App\Helpers\UAGTrade($sRow->profile->id);
			//$sUag->MoveFunds('2013564', '2013564');
			//exit;
			return View('backend.user.form')->with(array('sRow'=>$sRow) );
		/*} catch (\Exception $e) {*
			return redirect()->action('Backend\UserController@index')->with(['alert'=>\App\Models\Alert::e($e)]);
		}*/
    }

    public function destroy($id)
    {
		$sRow = \App\Models\User::find($id);
		if( $sRow ){
			\App\Models\User::where('id',$id)->forceDelete();
		}
		return response()->json(\App\Models\Alert::Msg('success'));
    }
	
	
	public function Datatable(){
		$sTable = \App\Models\User::search()->where('id','>','1');

		$sQuery	= DataTables::of($sTable)
		->addIndexColumn()
		->addColumn('full_name',function($data){
			return empty($data->first_name)?'-':$data->first_name.' '.$data->last_name;
		});
		return $sQuery->make(true);
	}
	
}
