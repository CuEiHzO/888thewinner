<?php
namespace App\Helpers;
class Cmd228{
    public $url = 'https://www.cmd228.net/Member/BetsView/BetLight/DataOdds.ashx';
    public function __construct()
    {

    }
    
    function run()
    {
		$fields = 'fc=1&m_accType=HK&SystemLanguage=th-TH&TimeFilter=0&m_gameType=S_&m_SortByTime=0&m_LeagueList=&SingleDouble=single&clientTime=&c=A&fav=&exlist=0&keywords=&m_sp=1';
		$fields = 'fc=6&m_accType=HK&SystemLanguage=th-TH&TimeFilter=0&m_gameType=S_&m_SortByTime=0&m_LeagueList=&SingleDouble=single&clientTime=&c=A&fav=&exlist=0&keywords=&m_sp=1';
		
        $header = array();
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] =  "Cache-Control: max-age=0";
        $header[] =  "Connection: keep-alive"; 
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: "; // browsers keep this blank.
		
        $this->curl = curl_init($this->url);
        curl_setopt($this->curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36');
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($this->curl, CURLOPT_SSLVERSION,1);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 15);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_URL, $this->url);
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $fields);
	    $sHtml = curl_exec($this->curl);
		//echo $sHtml;
		$sHtml = json_decode( $sHtml );
		
		if( !empty($sHtml) ){
			if( !empty($sHtml->data) ){
				foreach( $sHtml->data AS $i => $match ){
					$this->QueryMatche($match);
				}
			}
			if( !empty($sHtml->today) ){
				foreach( $sHtml->today AS $i => $match ){
					$this->QueryMatche($match);
				}
			}
		}
		echo '<pre>';
		print_r($sHtml);
		echo '</pre>';
		
	}
	
	
	function GetBallFormat($C) {
		if ($C == '0') {
			return '0.0';
		}
		$A = floor($C);
		
		if( strpos($C,'.25') ){
			return $A . '/' . $A . ".5";
		}else{
			if( strpos($C,'.75') ){
				return $A . ".5/" . ceil($C);
			}else{
				return $C;
			}
		}
	}
	
    function QueryMatche($match)
    {
		if( !empty($match[60]) && $match[60] == 'Soccer' && $match[33]=='1'){
			$odds_id		= $match[0];
			$league_id		= $match[5];
			$score_home		= $match[7];
			$score_away		= $match[8];
			$bet_no			= $match[33];
			$matche_id		= $match[34];
			$league_name	= $match[37];
			$matche_home	= $match[38];
			$matche_away	= $match[39];
			$matche_status	= $match[87];
			
			$matche_time	= $match[53].':00';
			$matche_date	= date('Y-m-d',strtotime($match[56].'/'.date('Y')));

			$sRow = \App\Models\Matche::where('matche_id',$matche_id)->first();
			if( empty($sRow) ){
				if($matche_status > 0) return false;
				$sRow = new \App\Models\Matche;
				$sRow->matche_id	= $matche_id;
				$sRow->odds_id		= $odds_id;
				$sRow->bet_no		= $bet_no;
				if( $matche_status == 0 ){
					$sRow->matche_date 	= $matche_date;
					$sRow->matche_time 	= $matche_time;
					$sRow->matche_start = $matche_date.' '.$matche_time;
				}
			}
			if( in_array( $matche_status, array('0','1','2')) ){
				$sRow->score_h_home		= $score_home;
				$sRow->score_h_away 	= $score_away;
			}
			if( in_array( $matche_status, array('0','1','2','3')) ){
				$sRow->score_home 		= $score_home;
				$sRow->score_away 		= $score_away;
				$sRow->hdp_h_a			= $match[44];
				$sRow->hdp_h_b			= $match[45];
				$sRow->ou_h_a			= $match[46];
				$sRow->ou_h_b			= $match[47];
				
				$sRow->odd_h			= $match[65];
				$sRow->even_h			= $match[66];
				
				$sRow->hdp_h_v 			= $match[14];
				$sRow->ou_h_v 			= $match[16];
				$sRow->hdp_h 			= $this->GetBallFormat($match[10]);
				$sRow->ou_h				= $this->GetBallFormat($match[12]);
			}
			if( in_array( $matche_status, array('0','1','2','3')) ){
				$sRow->hdp_f_a			= $match[40];
				$sRow->hdp_f_b			= $match[41];
				$sRow->ou_f_a			= $match[42];
				$sRow->ou_f_b			= $match[43];
				
				$sRow->odd_f			= $match[48];
				$sRow->even_f			= $match[49];
				
				$sRow->hdp_f_v 			= $match[10];
				$sRow->ou_f_v 			= $match[12];
				$sRow->hdp_f 			= $this->GetBallFormat($match[10]);
				$sRow->ou_f 			= $this->GetBallFormat($match[12]);
			}
			
			$sRow->league_id 		= $league_id;
			$sRow->league_name		= $league_name;
			$sRow->matche_home		= $matche_home;
			$sRow->matche_away		= $matche_away;
			$sRow->matche_status	= $matche_status;
			$sRow->save();
			
			

			$sRow= \App\Models\League::where('id',$league_id)->first();
			if( empty($sRow) ){
				$sRow= new \App\Models\League;
				$sRow->id 			= $league_id;
				$sRow->league_name 	= $league_name;
				$sRow->save();
			}
			//echo '<hr/>';
		}
	}
}

?>