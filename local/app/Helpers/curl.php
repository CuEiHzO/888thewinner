<?php
namespace App\Helpers;
class Curl{
    public $cookieJar = "";
    public function __construct($cookieJarFile = 'cookie.txt')
    {
        $this->cookieJar = storage_path($cookieJarFile.'.txt');
    }
    
    function Setup()
    {
        $header = array();
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] =  "Cache-Control: max-age=0";
        $header[] =  "Connection: keep-alive"; 
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: "; // browsers keep this blank.
        curl_setopt($this->curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36');
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($this->curl, CURLOPT_SSLVERSION,1);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($this->curl,CURLOPT_COOKIEJAR, $this->cookieJar);
        curl_setopt($this->curl,CURLOPT_COOKIEFILE, $this->cookieJar);
		
        //curl_setopt($this->curl,CURLOPT_AUTOREFERER, true);
        curl_setopt($this->curl,CURLOPT_TIMEOUT, 20);
		//curl_setopt($this->curl,CURLOPT_HEADER, true);

         //curl_setopt($this->curl,CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->curl,CURLOPT_RETURNTRANSFER, true);
    }
    
    function Get($url, $referer=NULL)
    { 
        $this->curl = curl_init($url);
        $this->setup();
		if( $referer )
        curl_setopt($this->curl, CURLOPT_REFERER, $referer);
        return $this->request();
    }
    
    function GetAll($reg,$str)
    {
        preg_match_all($reg,$str,$matches);
        return $matches[1];
    }
    
    function PostForm($url, $fields, $referer='')
    {
        $this->curl = curl_init($url);
        $this->setup();
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_REFERER, $referer);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $fields);
        return $this->request();
    }
    
    function GetInfo($info)
    {
        $info = empty($info) ? curl_getinfo($this->curl) : curl_getinfo($this->curl,$info);          
        return $info;     
    }      
    function request(){
	    return curl_exec($this->curl);
    }
	
	function getFormFields($data,$action)
	{
		if (preg_match('/(<form.*?action=.?'.$action.'.*?<\/form>)/is', $data, $matches)) {
			$inputs = getInputs($matches[1]);

			return $inputs;
		} else {
			die('didnt find login form');
		}
	}

	function getInputs($form)
	{
		$inputs = array();
		$elements = preg_match_all('/(<input[^>]+>)/is', $form, $matches);
		if ($elements > 0) {
			for($i = 0; $i < $elements; $i++) {
				$el = preg_replace('/\s{2,}/', ' ', $matches[1][$i]);

				if (preg_match('/name=(?:["\'])?([^"\'\s]*)/i', $el, $name)) {
					$name  = $name[1];
					$value = '';

					if (preg_match('/value=(?:["\'])?([^"\'\s]*)/i', $el, $value)) {
						$value = $value[1];
					}

					$inputs[$name] = $value;
				}
			}
		}
		return $inputs;
	}
}