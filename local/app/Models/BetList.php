<?php

namespace App\Models;

use App\Models\InitModel;

class BetList extends InitModel
{
    protected $table = 'ck_bet_list';
    protected $primaryKey	= 'id';
	
    public function match()
    {
        return $this->belongsTo('App\Models\Matche', 'matche_id', 'id');
    }
}
