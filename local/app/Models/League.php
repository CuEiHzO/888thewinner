<?php

namespace App\Models;

use App\Models\InitModel;

class League extends InitModel
{
    protected $table = 'ck_league';
    protected $primaryKey	= 'id';
}
