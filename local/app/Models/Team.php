<?php

namespace App\Models;

use App\Models\InitModel;

class Team extends InitModel
{
    protected $table = 'ck_team';
    protected $primaryKey	= 'id';
	
    public function league()
    {
        return $this->belongsTo('App\Models\League', 'league_id', 'id');
    }
}
