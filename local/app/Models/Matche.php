<?php

namespace App\Models;

use App\Models\InitModel;

class Matche extends InitModel
{
    protected $table = 'ck_matche';
    protected $primaryKey	= 'id';
	
    public function league()
    {
        return $this->belongsTo('App\Models\League', 'league_id', 'id');
    }
}
