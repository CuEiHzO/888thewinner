<?php

namespace App\Models;

use App\Models\InitModel;

class Bet extends InitModel
{
    protected $table = 'ck_bet';
    protected $primaryKey	= 'id';
	
    public function list()
    {
        return $this->hasMany('App\Models\BetList', 'bet_id');
    }
}
