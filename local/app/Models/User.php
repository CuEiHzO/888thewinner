<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
	protected $table = 'ck_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
	
	public function isAdmin(){
		return $this->user_type === 'Admin';
	}
	
    public function scopesearch($sQuery)
    {
			
		if( request('Where') ){
			foreach(request('Where') AS $sKey => $sValue){
				if( $sValue ){
					if( strpos($sKey,'.') ){
						list($sModel, $sField) = explode('.',$sKey);
						$sQuery->whereHas($sModel, function ($query) use ($sField, $sValue) {
							$query->where($sField, $sValue);
						});
					}else{
						$sQuery->where($sKey, $sValue);
					}
				}
			}
		}
		
		if( request('Like') ){
			foreach(request('Like') AS $sKey => $sValue){
				if( $sValue ){
					if( strpos($sKey,'.') ){
						list($sModel, $sField) = explode('.',$sKey);
						$sQuery->whereHas($sModel, function ($query) use ($sField, $sValue) {
							$query->where($sField, 'like', '%'.$sValue.'%');
						});
					}else{
						$sQuery->where($sKey, 'like', '%'.$sValue.'%');
					}
				}
			}
		}
		
        return $sQuery;
    }
	

    public function profile()
    {
        return $this->hasOne('App\Models\Profile', 'users_id', 'id');
    }
}
