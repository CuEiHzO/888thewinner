
        <aside class="sidebar">
            <nav class="sidebar-nav">
                <ul class="metismenu">
                    <li class="title">
                        MAIN NAVIGATION
                    </li>

					@php
					if(!empty(Auth::user())){
						$Menu = new \App\Models\Menu;
						echo $Menu->setMenu(Request::path());
					}
					@endphp
					
                    <li>
                        <a href="logout">
                            <i class="material-icons">exit_to_app</i>
                            <span class="nav-label">Logout</span>
                        </a>
                    </li>
					<!--
					@can('isAdmin')
                    <li>&nbsp;</li>
					@if( Request::segment(1) == 'member' )
                    <li>
                        <a href="admin/">
                            <i class="material-icons col-primary">donut_large</i>
                            <span class="nav-label">สลับไปหน้าผู้ดูแลระบบ</span>
                        </a>
                    </li>
					@else
                    <li>
                        <a href="member/">
                            <i class="material-icons col-warning">donut_large</i>
                            <span class="nav-label">สลับไปหน้าลูกค้า</span>
                        </a>
                    </li>
					@endif
					@endcan
					-->
                </ul>
            </nav>
        </aside>