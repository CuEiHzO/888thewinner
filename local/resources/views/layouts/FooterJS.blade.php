	@if (session('alert'))
	<div id="toastr-alert" style="display:none;" rel="{{ strtolower(session('alert.status')) }}">{!! session('alert.msg') !!}</div>
	@endif
			
    <!-- Jquery Core Js -->
    <script src="themes/adminbsb/assets/plugins/jquery/dist/jquery.min.js"></script>
    <script src="themes/adminbsb/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="themes/adminbsb/assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
	
    <!-- JQuery Datatables Js -->
    <script src="themes/adminbsb/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="themes/adminbsb/assets/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>

    <!-- Pace Loader Js -->
    <script src="themes/adminbsb/assets/plugins/pace/pace.js"></script>

    <!-- Screenfull Js -->
    <script src="themes/adminbsb/assets/plugins/screenfull/src/screenfull.js"></script>

    <!-- Metis Menu Js -->
    <script src="themes/adminbsb/assets/plugins/metisMenu/dist/metisMenu.js"></script>

    <!-- Jquery Slimscroll Js -->
    <script src="themes/adminbsb/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Switchery Js -->
    <script src="themes/adminbsb/assets/plugins/switchery/dist/switchery.js"></script>

	<!-- Masked Input Js -->
    <script src="themes/adminbsb/assets/plugins/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>

    <!-- Toastr Js -->
    <script src="themes/adminbsb/assets/plugins/toastr/toastr.js"></script>

    <!-- waitMe Js -->
    <script src="themes/adminbsb/assets/plugins/wait-me/src/waitMe.js"></script>

    <!-- Custom Js -->
    <script src="themes/adminbsb/assets/js/script.js"></script>