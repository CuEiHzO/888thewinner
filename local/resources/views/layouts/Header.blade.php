
	<title>{{ config('app.name') }} {{ $sTitle ?? '' }}</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<base href="{{ asset('') }}">
	
    <!-- Favicon -->
    <link rel="icon" href="themes/adminbsb/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="themes/adminbsb/assets/plugins/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
    <link href="themes/adminbsb/assets/plugins/jquery-ui/themes/base/jquery-ui.min.css" rel="stylesheet" />
    <!-- DataTables Css -->
    <link href="themes/adminbsb/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	
    <!-- Animate.css Css -->
    <link href="themes/adminbsb/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Font Awesome Css -->
    <link href="themes/adminbsb/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- iCheck Css -->
    <link href="themes/adminbsb/assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />

    <!-- Switchery Css -->
    <link href="themes/adminbsb/assets/plugins/switchery/dist/switchery.css" rel="stylesheet" />

    <!-- Toastr Css -->
    <link href="themes/adminbsb/assets/plugins/toastr/toastr.css" rel="stylesheet" />

    <!-- WaitMe Css -->
    <link href="themes/adminbsb/assets/plugins/wait-me/src/waitMe.css" rel="stylesheet" />

    <!-- Metis Menu Css -->
    <link href="themes/adminbsb/assets/plugins/metisMenu/dist/metisMenu.css" rel="stylesheet" />

    <!-- Pace Loader Css -->
    <link href="themes/adminbsb/assets/plugins/pace/themes/white/pace-theme-flash.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="themes/adminbsb/assets/css/style.css" rel="stylesheet" />
    <link href="themes/adminbsb/assets/css/custom.css" rel="stylesheet" />

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="themes/adminbsb/assets/css/themes/allthemes.css" rel="stylesheet" />
	
	

	
@stack('css')