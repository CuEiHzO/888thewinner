
        <aside class="sidebar">
            <nav class="sidebar-nav">
                <ul class="metismenu">
                    <li class="title">
                        MAIN NAVIGATION
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">dashboard</i>
                            <span class="nav-label">Dashboards</span>
                        </a>
                        <ul>
                            <li>
                                <a href="themes/adminbsb/index.html">Dashboard v1</a>
                            </li>
                            <li>
                                <a href="../dashboard/dashboard-2.html">Dashboard v2</a>
                            </li>
                            <li>
                                <a href="../dashboard/dashboard-3.html">Dashboard v3</a>
                            </li>
                            <li>
                                <a href="../dashboard/dashboard-4.html">Dashboard v4</a>
                            </li>
                            <li>
                                <a href="../dashboard/dashboard-5.html">Dashboard v5</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="../typography.html">
                            <i class="material-icons">text_fields</i>
                            <span class="nav-label">Typography</span>
                        </a>
                    </li>
                    <li>
                        <a href="../helper-classes.html">
                            <i class="material-icons">layers</i>
                            <span class="nav-label">Helper Classes</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span class="nav-label">Widgets</span>
                        </a>
                        <ul>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Infobox</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="../widgets/infobox/infobox-1.html">Infobox v1</a>
                                    </li>
                                    <li>
                                        <a href="../widgets/infobox/infobox-2.html">Infobox v2</a>
                                    </li>
                                    <li>
                                        <a href="../widgets/infobox/infobox-3.html">Infobox v3</a>
                                    </li>
                                    <li>
                                        <a href="../widgets/infobox/infobox-4.html">Infobox v4</a>
                                    </li>
                                    <li>
                                        <a href="../widgets/infobox/infobox-5.html">Infobox v5</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Panels</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="../widgets/panels/basic.html">Basic</a>
                                    </li>
                                    <li>
                                        <a href="../widgets/panels/colored.html">Colored</a>
                                    </li>
                                    <li>
                                        <a href="../widgets/panels/draggable.html">Draggable</a>
                                    </li>
                                    <li>
                                        <a href="../widgets/panels/no-header.html">No Header</a>
                                    </li>
                                    <li>
                                        <a href="../widgets/panels/panel-with-loading.html">Panel With Loading</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="../widgets/weather-station.html">Weather Station</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">email</i>
                            <span class="nav-label">Mailbox</span>
                        </a>
                        <ul>
                            <li>
                                <a href="../mailbox/inbox.html">Inbox</a>
                            </li>
                            <li>
                                <a href="../mailbox/compose-email.html">Compose Email</a>
                            </li>
                            <li>
                                <a href="../mailbox/email-templates.html">Email Templates</a>
                            </li>
                            <li>
                                <a href="../mailbox/email-view.html">Email View</a>
                            </li>
                            <li>
                                <a href="../mailbox/outlook-view.html">Outlook View</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span class="nav-label">User Interface (UI)</span>
                        </a>
                        <ul>
                            <li>
                                <a href="../ui/accordion.html">Accordion</a>
                            </li>
                            <li>
                                <a href="../ui/alerts.html">Alerts</a>
                            </li>
                            <li>
                                <a href="../ui/animations.html">Animations</a>
                            </li>
                            <li>
                                <a href="../ui/badges.html">Badges</a>
                            </li>
                            <li>
                                <a href="../ui/breadcrumbs.html">Breadcrumbs</a>
                            </li>
                            <li>
                                <a href="../ui/buttons.html">Buttons</a>
                            </li>
                            <li>
                                <a href="../ui/collapse.html">Collapse</a>
                            </li>
                            <li>
                                <a href="../ui/colors.html">Colors</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Icons</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="../ui/icons/font-awesome.html">Font Awesome</a>
                                    </li>
                                    <li>
                                        <a href="../ui/icons/google-material-icons.html">Google Material Icons</a>
                                    </li>
                                    <li>
                                        <a href="../ui/icons/skycons.html">Skycons</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="../ui/labels.html">Labels</a>
                            </li>
                            <li>
                                <a href="../ui/list-group.html">List Group</a>
                            </li>
                            <li>
                                <a href="../ui/media-object.html">Media Object</a>
                            </li>
                            <li>
                                <a href="../ui/modals.html">Modals</a>
                            </li>
                            <li>
                                <a href="../ui/notifications.html">Notifications</a>
                            </li>
                            <li>
                                <a href="../ui/pagination-pager.html">Pagination & Pager</a>
                            </li>
                            <li>
                                <a href="../ui/progress-bars.html">Progress Bars</a>
                            </li>
                            <li>
                                <a href="../ui/sortable-nestable.html">Sortable & Nestable</a>
                            </li>
                            <li>
                                <a href="../ui/tabs.html">Tabs</a>
                            </li>
                            <li>
                                <a href="../ui/thumbnails.html">Thumbnails</a>
                            </li>
                            <li>
                                <a href="../ui/tooltips-popovers.html">Tooltips & Popovers</a>
                            </li>
                            <li>
                                <a href="../ui/video.html">Video</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span class="nav-label">Forms</span>
                        </a>
                        <ul>
                            <li>
                                <a href="../forms/basic-form-elements.html">Basic Form Elements</a>
                            </li>
                            <li>
                                <a href="../forms/advanced-form-elements.html">Advanced Form Elements</a>
                            </li>
                            <li>
                                <a href="../forms/form-examples.html">Form Examples</a>
                            </li>
                            <li>
                                <a href="../forms/form-validation.html">Form Validation</a>
                            </li>
                            <li>
                                <a href="../forms/form-wizard.html">Form Wizard</a>
                            </li>
                            <li>
                                <a href="../forms/editors.html">Editors</a>
                            </li>
                            <li>
                                <a href="../forms/autocomplete.html">Autocomplete</a>
                            </li>
                            <li>
                                <a href="../forms/markdown.html">Markdown</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Image Cropper</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="../forms/image-cropper/image-cropper-1.html">Image Cropper v1</a>
                                    </li>
                                    <li>
                                        <a href="../forms/image-cropper/image-cropper-2.html">Image Cropper v2</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">view_list</i>
                            <span class="nav-label">Tables</span>
                        </a>
                        <ul>
                            <li>
                                <a href="../tables/normal-tables.html">Normal Tables</a>
                            </li>
                            <li>
                                <a href="../tables/jquery-datatables.html">Jquery DataTables</a>
                            </li>
                            <li>
                                <a href="../tables/editable-table.html">Editable Tables</a>
                            </li>
                            <li>
                                <a href="../tables/footable.html">Footable</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">perm_media</i>
                            <span class="nav-label">Medias</span>
                        </a>
                        <ul>
                            <li>
                                <a href="../medias/carousel.html">Carousel</a>
                            </li>
                            <li>
                                <a href="../medias/content-slider.html">Content Slider</a>
                            </li>
                            <li>
                                <a href="../medias/grid-masonry.html">Grid Masonry</a>
                            </li>
                            <li>
                                <a href="../medias/image-comparison-slider.html">Image Comparison Slider</a>
                            </li>
                            <li>
                                <a href="../medias/lightbox-image-gallery.html">Lightbox Image Gallery</a>
                            </li>
                            <li>
                                <a href="../medias/owl-carousel.html">Owl Carousel</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">pie_chart</i>
                            <span class="nav-label">Charts</span>
                        </a>
                        <ul>
                            <li>
                                <a href="../charts/morris.html">Morris</a>
                            </li>
                            <li>
                                <a href="../charts/flot.html">Flot</a>
                            </li>
                            <li>
                                <a href="../charts/chartjs.html">ChartJS</a>
                            </li>
                            <li>
                                <a href="../charts/peity.html">Peity</a>
                            </li>
                            <li>
                                <a href="../charts/rickshaw.html">Rickshaw</a>
                            </li>
                            <li>
                                <a href="../charts/sparkline.html">Sparkline</a>
                            </li>
                            <li>
                                <a href="../charts/jquery-knob.html">Jquery Knob</a>
                            </li>
                            <li>
                                <a href="../charts/c3.html">C3</a>
                            </li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">content_copy</i>
                            <span class="nav-label">Example Pages</span>
                        </a>
                        <ul>
                            <li>
                                <a href="../examples/sign-in.html">Sign In</a>
                            </li>
                            <li>
                                <a href="../examples/sign-up.html">Sign Up</a>
                            </li>
                            <li>
                                <a href="../examples/forgot-password.html">Forgot Password</a>
                            </li>
                            <li class="active">
                                <a href="../examples/blank-page.html">Blank Page</a>
                            </li>
                            <li>
                                <a href="../examples/locked-screen.html">Locked Screen</a>
                            </li>
                            <li>
                                <a href="../examples/maintenance.html">Maintenance</a>
                            </li>
                            <li>
                                <a href="../examples/search-result.html">Search Result</a>
                            </li>
                            <li>
                                <a href="../examples/invoice.html">Invoice</a>
                            </li>
                            <li>
                                <a href="../examples/timeline.html">Timeline</a>
                            </li>
                            <li>
                                <a href="../examples/403.html">403 - Forbidden</a>
                            </li>
                            <li>
                                <a href="../examples/404.html">404 - Not Found</a>
                            </li>
                            <li>
                                <a href="../examples/500.html">500 - Server Error</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">map</i>
                            <span class="nav-label">Maps</span>
                        </a>
                        <ul>
                            <li>
                                <a href="../maps/google-maps.html">Google Map</a>
                            </li>
                            <li>
                                <a href="../maps/yandex-maps.html">Yandex Map</a>
                            </li>
                            <li>
                                <a href="../maps/vector-map.html">jVectorMap</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">dvr</i>
                            <span class="nav-label">Miscellaneous</span>
                        </a>
                        <ul>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Contacts</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="../miscellaneous/contacts/contact-detail.html">Contact Detail</a>
                                    </li>
                                    <li>
                                        <a href="../miscellaneous/contacts/contact-grid.html">Contact Grid</a>
                                    </li>
                                    <li>
                                        <a href="../miscellaneous/contacts/contact-list.html">Contact List</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="../miscellaneous/agile-board.html">Agile Board</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/bootstrap-tour.html">Bootstrap Tour</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/calendar.html">Calendar</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/chat-dashboard.html">Chat Dashboard</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/code-editor.html">Code Editor</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/faq.html">FAQ</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/favicon-bubble.html">Favicon Bubble</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/file-manager.html">File Manager</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/i18n.html">i18n Multi-language</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/idle-timer.html">Idle Timer</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/password-strength-meter.html">Password Strength Meter</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/profile.html">Profile</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/text-diff.html">Text Diff</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/todo-list.html">ToDo List</a>
                            </li>
                            <li>
                                <a href="../miscellaneous/tree-view.html">Tree View</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">rss_feed</i>
                            <span class="nav-label">Blog</span>
                        </a>
                        <ul>
                            <li>
                                <a href="../blog/single-post.html">Single Post</a>
                            </li>
                            <li>
                                <a href="../blog/posts.html">Posts</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">shopping_cart</i>
                            <span class="nav-label">E-Commerce</span>
                        </a>
                        <ul>
                            <li>
                                <a href="../ecommerce/cart.html">Cart</a>
                            </li>
                            <li>
                                <a href="../ecommerce/orders.html">Orders</a>
                            </li>
                            <li>
                                <a href="../ecommerce/payment.html">Payment</a>
                            </li>
                            <li>
                                <a href="../ecommerce/pricing-table.html">Pricing Table</a>
                            </li>
                            <li>
                                <a href="../ecommerce/product-detail.html">Product Detail</a>
                            </li>
                            <li>
                                <a href="../ecommerce/product-edit.html">Product Edit</a>
                            </li>
                            <li>
                                <a href="../ecommerce/product-grid.html">Product Grid</a>
                            </li>
                            <li>
                                <a href="../ecommerce/product-list.html">Product List</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">trending_down</i>
                            <span class="nav-label">Multi Level Menu</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="javascript:void(0);">Menu Item - 1</a></li>
                            <li><a href="javascript:void(0);">Menu Item - 2</a></li>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Level - 2</span>
                                </a>
                                <ul>
                                    <li><a href="javascript:void(0);">Menu Item - 1</a></li>
                                    <li><a href="javascript:void(0);">Menu Item - 2</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:void(0);">Menu Item - 4</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="../changelogs.html">
                            <i class="material-icons">update</i>
                            <span class="nav-label">Changelogs</span>
                        </a>
                    </li>
                    <li>
                        <a href="themes/adminbsb/documents/index.html">
                            <i class="material-icons">book</i>
                            <span class="nav-label">Documents</span>
                        </a>
                    </li>
                    <li class="title">
                        LABELS
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-danger">donut_large</i>
                            <span class="nav-label">Important</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-warning">donut_large</i>
                            <span class="nav-label">Warning</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-info">donut_large</i>
                            <span class="nav-label">Information</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </aside>