@extends('layouts')

@section('content')
<section class="content">
	<div class="page-heading">
		<h1>จัดการลีคฟุตบอล</h1>
		<ol class="breadcrumb">
			<li><a href="admin">Home</a></li>
			<li><a href="javascript:void(0);">Backend</a></li>
			<li class="active">จัดการลีคฟุตบอล</li>
		</ol>
	</div>
	<div class="page-body">

		<div class="panel panel-default">
			<div class="panel-body">
				<form class="form-horizontal">
					<div class="form-group m-b-0">
						<div class="col-sm-8">
							<input type="text" class="form-control pull-left align-center w200 myLike " placeholder="Name" name="league_name">
						</div>
						<div class="col-sm-4 align-right"></div>
					</div>
				</form>
				
				
				<table id="data-table" class="table table-bordered" width="100%"></table>
			</div>
		</div>

	</div>
</section>
@endsection

@push('css')
<link href="themes/adminbsb/assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet" />
<link href="themes/adminbsb/assets/plugins/iCheck/skins/square/_all.css" rel="stylesheet" />
<link href="themes/adminbsb/assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet" />
@endpush

@push('scripts')
<script src="themes/adminbsb/assets/plugins/iCheck/icheck.js"></script>
<script>
$(function() {
	oTable = $('#data-table').DataTable({
		"sDom": "<'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        processing: true,
        serverSide: true,
        scroller: true,
		scrollCollapse: true,
        scrollX: true,
		ordering: false,
		scrollY: ''+($(window).height()-370)+'px',
		iDisplayLength: 25,
        ajax: {
			url: '{{ action("Backend\LeagueController@Datatable") }}',
			data: function ( d ) {
				d.Where={};
				$('.myWhere').each(function() {
					if( $.trim($(this).val()) && $.trim($(this).val()) != '0' ){
						d.Where[$(this).attr('name')] = $.trim($(this).val());
					}
				});
				d.Like={};
				$('.myLike').each(function() {
					if( $.trim($(this).val()) && $.trim($(this).val()) != '0' ){
						d.Like[$(this).attr('name')] = $.trim($(this).val());
					}
				});
				oData = d;
			},
            method: 'POST'
        },
        columns: [
            {data: 'id', title :'#', className: 'text-center w50'},
            {data: 'league_name', title :'Name', className: ''},
            {data: 'league_status', title :'สถานะ', className: 'text-center w50'},
            {data: 'created_at', title :'Created', className: 'text-center w150', defaultContent: '-'},
            {data: 'updated_at', title :'Updated', className: 'text-center w150', defaultContent: '-'},
        ],
		rowCallback: function(nRow, aData, dataIndex){
			$('td:eq(2)', nRow).html('<input type="checkbox" '+(aData['league_status']=='Y'?'checked':'')+' rel="admin/league/status/'+aData['id']+'" name="league_status" data-icheck-theme="square" data-icheck-color="blue" value="'+aData['id']+'">');
		},
		drawCallback: function( settings ) {
			$('input[data-icheck-theme]').each(function (i, key) {
				var color = $(key).data('icheckColor');
				var theme = $(key).data('icheckTheme');
				var baseCheckboxClass = 'icheckbox_' + theme;
				var baseRadioClass = 'iradio_' + theme;

				$(key).iCheck({
					checkboxClass: color === theme ? baseCheckboxClass : baseCheckboxClass + '-' + color,
					radioClass: color === theme ? baseRadioClass : baseRadioClass + '-' + color
				});
			});
		}
    });
	$('.myWhere,.myLike,#onlyTrashed').on('change', function(e){
		oTable.draw();
	});

	$( '#data-table' ).on( "ifChecked", 'input', function(event) {
		$.AdminBSB.Custom.toastr('info', 'กำลังเริ่มทำรายการ');
		$.ajax({
			url: $(this).attr('rel')+'/Y',
			type: 'POST',
			dataType: "JSON",
			success: function(response) {
				$.AdminBSB.Custom.toastr(response.status.toLowerCase(),response.msg);
			}
		});
	});
	
	$( '#data-table' ).on( "ifUnchecked", 'input', function(event) {
		$.AdminBSB.Custom.toastr('info', 'กำลังเริ่มทำรายการ');
		$.ajax({
			url: $(this).attr('rel')+'/N',
			type: 'POST',
			dataType: "JSON",
			success: function(response) {
				$.AdminBSB.Custom.toastr(response.status.toLowerCase(),response.msg);
			}
		});
	});
			
});
</script>
@endpush

