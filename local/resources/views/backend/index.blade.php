<!DOCTYPE html>
<html lang="en">
<head>
@include('layouts.Header')
</head>
<body class="ls-toggled">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
		@include('layouts.TopBar')
        <!-- #END# Top Bar -->

       
        <!-- #END# Right Sidebar -->
        <section class="content" style="margin:0px;">


            <div class="page-heading">
                <h1>DASHBOARD</h1>
            </div>

            <div class="page-body">
                <!-- Infobox -->
				<div class="row clearfix">
					<form id="frm" method="post" action="{{ action("Backend\DashboardController@store") }}">
					@csrf
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">Sports</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table js-exportable">
										<thead>
											<tr>
												<th>#</th>
												<th>Key</th>
												<th>Group</th>
												<th>Premier</th>
											</tr>
										</thead>
										<tbody>
										@if($sSports)
											@foreach($sSports AS $r)
											<tr>
												<td class="align-left">
													 <input type="checkbox" id="{{$r->sport_key}}" data-icheck-theme="flat" data-icheck-color="blue" name="sport_key[{{$r->sport_key}}]" value="Y" {!! ($r->sport_status=='Y')?'checked':''; !!} >
												</td>
												<td class="align-left"><label for="{{$r->sport_key}}">{{$r->sport_key}}</label></td>
												<td class="align-left">{{$r->sport_group}}</td>
												<td class="align-left">{{$r->sport_title}}</td>
											</tr>
											@endforeach
										@endif
										</tbody>
									</table>
								</div>
								<div class="form-group clearfix">
									
									<button type="submit" class="btn btn-sm btn-success pull-right">ปรับปรุงโปรแกรมการแข่งขัน</button>
								</div>
							</div>
						</div>
					</div>
					</form>
					

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<!-- Task List -->
						<div class="panel panel-default">
							<div class="panel-heading">Live Soccer</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table js-exportable">
										<thead>
											<tr>
												<th>#</th>
												<th>Premier</th>
												<th class="align-center">Date-Time</th>
												<th class="align-center">เจ้าบ้าน</th>
												<th class="align-center">ทีมเยือน</th>
											</tr>
										</thead>
										<tbody>
										@if($sOdds)
											@foreach($sOdds AS $r)
											<tr>
												<td class="align-left">#</td>
												<td class="align-left">{{$r->sport_nice}}</td>
												<td class="align-center">{{ date('d/m/Y',$r->commence_time) }}<br/>{{ date('H:i:s',$r->commence_time) }}</td>
												<td class="align-center">{{$r->home_team}}</td>
												<td class="align-center">{{$r->away_team}}</td>
											</tr>
											@endforeach
										@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- #END# Task List -->
					</div>


				</div>
				
                
		
        </section>
    </div>
	@include('layouts.FooterJS')
	@stack('scripts')
</body>
</html>

