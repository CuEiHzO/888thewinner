@extends('layouts')

@section('content')
<section class="content">
	<div class="page-heading">
		<h1>จัดการแมทการแข่งขัน</h1>
		<ol class="breadcrumb">
			<li><a href="admin">Home</a></li>
			<li><a href="javascript:void(0);">Backend</a></li>
			<li class="active">จัดการแมทการแข่งขัน</li>
		</ol>
	</div>
	<div class="page-body">

		<div class="panel panel-default">
			<div class="panel-body">
				<form class="form-horizontal">
					<div class="form-group m-b-0">
						<div class="col-sm-12">
							<select class="form-control pull-left myWhere w250" name="league_id">
								<option value="">------------ Show All League ------------</option>
							@if($sLeague)
								@foreach($sLeague AS $l)
								<option value="{{ $l->id }}">{{ $l->league_name }}</option>
								@endforeach
							@endif
							</select>
							<input type="text" class="form-control pull-left align-center w150 myLike m-l-5" placeholder="Home" name="matche_home">
							<input type="text" class="form-control pull-left align-center w150 myLike m-l-5" placeholder="Away" name="matche_away">
							<input type="text" class="form-control pull-left align-center w150 myCustom m-l-5 datepicker" placeholder="Date" name="matche_date">
							<select class="form-control pull-left myWhere m-l-5 w125" name="matche_status">
								<option value="">แสดงทุกสถานะ</option>
								<option value="0">รอแข่งขัน</option>
								<option value="1">กำลังแข่งขัน</option>
								<option value="2">จบครึ่งแรก</option>
								<option value="3">กำลังแข่งขัน</option>
								<option value="4">จบแข่งขัน</option>
							</select>
						</div>
					</div>
				</form>
				<table id="data-table" class="table table-bordered" width="100%"></table>
			</div>
		</div>
		

	</div>
</section>




<div class="modal fade" id="modalDiv" >
	<div class="modal-dialog" >
		<div class="modal-content">
		<form id="formModal" action="#" autocomplete="off">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">เปลี่ยนแปลงข้อมูลการแข่งขัน</h4>
			</div>

			<div class="modal-body form-horizontal">

				<div class="form-group m-b-5">
					<center>
						<input type="text" class="form-control pull-left w220 myData m-l-5" name="matche_home" readonly/>
						<input type="text" class="form-control pull-left w40 myData m-l-5 text-center" name="score_home" value="0"/>
						<input type="text" class="form-control pull-left w40 m-l-5 text-center" value="vs" readonly/>
						<input type="text" class="form-control pull-left w40 myData m-l-5 text-center" name="score_away" value="0"/>
						<input type="text" class="form-control pull-left w220 myData m-l-5" name="matche_away" readonly/>
					</center>
				</div>
				<div class="form-group m-b-5 m-t-15">
					<center>
						<input type="text" class="form-control pull-left w90 m-l-5 text-center b" value="ราคา" readonly/>
						<input type="text" class="form-control pull-left w90 m-l-5 text-center b" value="เจ้าบ้าน" readonly/>
						<input type="text" class="form-control pull-left w90 m-l-5 text-center b" value="ทีมเยือน" readonly/>
						<input type="text" class="form-control pull-left w90 m-l-20 text-center b" value="ราคา" readonly/>
						<input type="text" class="form-control pull-left w90 m-l-5 text-center b" value="สูง" readonly/>
						<input type="text" class="form-control pull-left w90 m-l-5 text-center b" value="ต่ำ" readonly/>
					</center>
				</div>
				<div class="form-group m-b-5">
					<center>
					
						<input type="text" class="form-control pull-left w90 m-l-5 text-center myData" name="handicap_val" placeholder="ราคา" />
						<input type="text" class="form-control pull-left w90 m-l-5 text-center myData" name="handicap_home" placeholder="เจ้าบ้าน" />
						<input type="text" class="form-control pull-left w90 m-l-5 text-center myData" name="handicap_away" placeholder="ทีมเยือน" />
						<input type="text" class="form-control pull-left w90 m-l-20 text-center myData" name="goals_total" placeholder="ราคา" />
						<input type="text" class="form-control pull-left w90 m-l-5 text-center myData" name="goals_over" placeholder="สูง" />
						<input type="text" class="form-control pull-left w90 m-l-5 text-center myData" name="goals_under" placeholder="ต่ำ" />
					</center>
				</div>
		
			</div>
		
			<div class="modal-footer">
				<div class="col-md-4 text-left"></div>
				<div class="col-md-4 text-center"></div>
				<div class="col-md-4 text-right">
					<input type="hidden" id="id" name="id" class="myData"/>
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<button type="submit" class="btn btn-sm btn-primary m-r-5 Save">Save</button>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>





@endsection



@push('css')
<style>
.bisque{background-color: bisque;}
</style>
@endpush


@push('scripts')
<script>
var oTable;
$(function() {
	
	$('.datepicker').datepicker({
		showButtonPanel: true,
		todayHighlight: true,
		changeYear: true,
		closeText: 'Clear',
		dateFormat: 'dd-mm-yy',
		yearRange: "1950:+2",
		//defaultDate: '+543y',
		onClose: function (dateText, inst) {
			if ($(window.event.srcElement).hasClass('ui-datepicker-close')) {
				document.getElementById(this.id).value = '';
			}
		}
	});

	oTable = $('#data-table').DataTable({
		"sDom": "<'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        processing: true,
        serverSide: true,
        scroller: true,
		scrollCollapse: true,
        scrollX: true,
		ordering: false,
		scrollY: ''+($(window).height()-370)+'px',
		iDisplayLength: 25,
        ajax: {
			url: '{{ action("Backend\MatchController@Datatable") }}',
			data: function ( d ) {
				d.Where={};
				$('.myWhere').each(function() {
					if( $.trim($(this).val()) && $.trim($(this).val()) != '' ){
						d.Where[$(this).attr('name')] = $.trim($(this).val());
					}
				});
				d.Like={};
				$('.myLike').each(function() {
					if( $.trim($(this).val()) && $.trim($(this).val()) != '0' ){
						d.Like[$(this).attr('name')] = $.trim($(this).val());
					}
				});
				d.Custom={};
				$('.myCustom').each(function() {
					if( $.trim($(this).val()) && $.trim($(this).val()) != '0' ){
						d.Custom[$(this).attr('name')] = $.trim($(this).val());
					}
				});
				oData = d;
			},
            method: 'POST'
        },
        columns: [
            {data: 'matche_start', title :'Date Time', className: 'text-center w120'},
            {data: 'league_name', title :'League', className: 'text-center'},
            {data: 'matche_home', title :'Home', className: 'text-center'},
            {data: 'score', title :'Score', className: 'text-center w50'},
            {data: 'matche_away', title :'Away', className: 'text-center'},
			
            {data: 'hdp_f', title :'ราคา', className: 'text-center w50 bisque'},
            {data: 'hdp_f_a', title :'เจ้าบ้าน', className: 'text-center w50'},
            {data: 'hdp_f_b', title :'ทีมเยือน', className: 'text-center w50'},
            {data: 'ou_f', title :'ราคา', className: 'text-center w50 bisque'},
            {data: 'ou_f_a', title :'สูง', className: 'text-center w50'},
            {data: 'ou_f_b', title :'ต่ำ', className: 'text-center w50'},
            {data: 'matche_status', title :'สถานะ', className: 'text-center w70'},
			
            {data: 'updated_at', title :'Last Update', className: 'text-center w120'},
        ],
		rowCallback: function(nRow, aData, dataIndex){
			$(nRow).data('aData',  aData);
		}
    });
	$('.myWhere,.myLike,.myCustom,#onlyTrashed').on('change', function(e){
		oTable.draw();
	});
	

	$( '#data-table' ).on( "click", 'tbody tr td', function() {
		var self = $(this);
		if( self.parent().data('aData').id ){
			$('#formModal').trigger("reset");
			$('#modalDiv').modal('show');
			frm.PutForm('myData', self.parent().data('aData'));
		}				
	});
	
	
	$('#formModal').submit(function(event) {
		event.preventDefault();
		$('#modalDiv').modal('hide');
		if(confirm('ยืนยันการเปลี่ยนแปลงข้อมูลผลการแข่งขัน ?') == true){
			$.AdminBSB.Custom.toastr('info', 'เริ่มทำการส่งข้อมูล..');
			$.ajax({
				type: 'PUT',
				dataType: 'JSON',
				url: '{{ action("Backend\MatchController@index") }}/'+$('#id').val(),
				data: frm.LoadForm('myData'),
				success: function(result) {
					if( result.status == 'Success' ){
						$.AdminBSB.Custom.toastr('success', result.msg);
						oTable.draw(false);
					}else{
						$.AdminBSB.Custom.toastr('error', result.msg);
					}
				},
				error: function(xhr, error){
					$.AdminBSB.Custom.toastr('error', xhr.statusText, 'Error code : '+xhr.status);
				 }
			});
		}
	});
	
	
});
</script>
@endpush

