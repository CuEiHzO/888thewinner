@extends('layouts')

@section('content')
<section class="content">
	<div class="page-heading">
		<h1>ผู้ใช้งาน</h1>
		<ol class="breadcrumb">
			<li><a href="admin">Home</a></li>
			<li><a href="javascript:void(0);">Backend</a></li>
			<li class="active">ผู้ใช้งาน</li>
		</ol>
	</div>
	<div class="page-body">

		<div class="panel panel-default">
			<div class="panel-body">
				<form class="form-horizontal">
					<div class="form-group m-b-0">
						<div class="col-sm-8">
							<input type="text" class="form-control pull-left align-center w130 myLike" placeholder="Username" name="username">
						</div>
						<div class="col-sm-4 align-right">
                            <button type="button" class="btn btn-info btn-sm">เพิ่มผู้ใช้งาน</button>
						</div>
					</div>
				</form>
				<table id="data-table" class="table table-bordered" width="100%"></table>
			</div>
		</div>
		

	</div>
</section>
@endsection



@push('scripts')
<script>
$(function() {
		oTable = $('#data-table').DataTable({
		"sDom": "<'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        processing: true,
        serverSide: true,
        scroller: true,
		scrollCollapse: true,
        scrollX: true,
		ordering: false,
		scrollY: ''+($(window).height()-370)+'px',
		iDisplayLength: 25,
        ajax: {
			url: '{{ action("Backend\UserController@Datatable") }}',
			data: function ( d ) {
				d.Where={};
				$('.myWhere').each(function() {
					if( $.trim($(this).val()) && $.trim($(this).val()) != '0' ){
						d.Where[$(this).attr('name')] = $.trim($(this).val());
					}
				});
				d.Like={};
				$('.myLike').each(function() {
					if( $.trim($(this).val()) && $.trim($(this).val()) != '0' ){
						d.Like[$(this).attr('name')] = $.trim($(this).val());
					}
				});
				oData = d;
			},
            method: 'POST'
        },
        columns: [
            {data: 'DT_RowIndex', title :'#', className: 'text-center w30'},
            {data: 'username', title :'Username', className: 'text-center w120'},
            {data: 'first_name', title :'First Name', className: 'text-center'},
            {data: 'last_name', title :'Last Name', className: 'text-center'},
            {data: 'email', title :'E-mail', className: 'text-center'},
            {data: 'phone', title :'Phone', className: 'text-center'},
            {data: 'id', title :'แก้ไข', className: 'text-center w110'},
        ],
		rowCallback: function(nRow, aData, dataIndex){
			$('td:last-child', nRow).html(''
				+ '<a href="{{ action("Backend\UserController@index") }}/'+aData['id']+'/edit'+(aData['deleted_at']?'?withTrashed=true':'')+'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> แก้ไข</a> '
				+((aData['id']<3)?'':'<a rel="{{ action("Backend\UserController@index") }}/'+aData['id']+'/" class="btn btn-xs btn-danger cDelete" ><i class="glyphicon glyphicon-remove"></i> ลบ</a>')
			).addClass('input');
		}
    });
	$('.myWhere,.myLike,#onlyTrashed').on('change', function(e){
		oTable.draw();
	});
});
</script>
@endpush

