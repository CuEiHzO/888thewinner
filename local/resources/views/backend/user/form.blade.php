@extends('layouts')

@section('content')
<section class="content">
	<div class="page-heading">
		<h1>ผู้ใช้งาน</h1>
		<ol class="breadcrumb">
			<li><a href="../../index.html">Home</a></li>
			<li><a href="javascript:void(0);">Backend</a></li>
			<li class="active">ผู้ใช้งาน</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row clearfix">
		
		
		
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
				<div class="panel panel-default">
					<div class="panel-heading">User Information</div>
					<div class="panel-body">
						<form action="@if(empty($sRow)) {{ action('Backend\UserController@store') }}@else {{action('Backend\UserController@update', $sRow->id )}}@endif" method="POST" autocomplete="off">
						{{ csrf_field() }}
						@if( !empty($sRow) )<input name="_method" type="hidden" value="PUT">@endif
							<div class="col-md-12 col-lg-12">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Username :</label>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-user"></i>
											</span>
											@if( empty($sRow) )
											<input type="text" class="form-control" placeholder="Username" name="username" required />
											@else
											<input type="text" class="form-control" value="{{ $sRow->username??'' }}" readonly>
											@endif
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Password :</label>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-lock"></i>
											</span>
											<input type="text" class="form-control" placeholder="Password" name="password" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-lg-12">
								<div class="col-sm-6">
									<div class="form-group">
										<label>First Name :</label>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-user"></i>
											</span>
											<input type="text" class="form-control" placeholder="First Surname" value="{{ $sRow->first_name??'' }}" name="first_name" />
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Last Name :</label>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-user"></i>
											</span>
											<input type="text" class="form-control" placeholder="Last Name" value="{{ $sRow->last_name??'' }}" name="last_name" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-lg-12">
								<div class="col-sm-12">
									<div class="form-group">
										<label>E-mail :</label>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-envelope-o"></i>
											</span>
											<input type="text" class="form-control" placeholder="E-mail" value="{{ $sRow->email??'' }}" name="email" />
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label>Phone :</label>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-phone"></i>
											</span>
											<input type="text" class="form-control" placeholder="Phone" value="{{ $sRow->phone??'' }}" name="phone" />
										</div>
									</div>
								</div>
								<div class="col-sm-12 m-t-10 m-b-15">
									<button type="submit" class="btn btn-success">บันทึก</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			

		</div>
	</div>
</section>
@endsection



@push('scripts')
<script>
$(function() {

});
</script>
@endpush

