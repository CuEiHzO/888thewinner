<!DOCTYPE html>
<html lang="en">
<head>
@include('layouts.Header')
</head>
<body class="ls-toggled">
    <div class="all-content-wrapper">
		
        <!-- #END# Right Sidebar -->
        <section class="content" style="margin:0px;">


            <div class="page-heading">
                <h1>DASHBOARD</h1>
            </div>

            <div class="page-body">
                <!-- Task List -->
                <div class="panel panel-default">
                    <div class="panel-heading">Live Soccer</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table js-exportable">
                                <thead>
                                    <tr>
										<th>#</th>
										<th>Premier</th>
										<th class="align-center">Date-Time</th>
										<th class="align-center">เจ้าบ้าน</th>
										<th class="align-center">ทีมเยือน</th>
										<th class="align-center">-</th>
										<th class="align-center">ราคา</th>
										<th class="align-center">A</th>
										<th class="align-center">B</th>
                                    </tr>
                                </thead>
                                <tbody>
								@if($sRow)
									@foreach($sRow AS $r)
                                    <tr>
                                        <td class="align-left">#</td>
                                        <td class="align-left">{{$r->sport_nice}}</td>
                                        <td class="align-center">{{ date('d/m/Y',$r->commence_time) }}<br/>{{ date('H:i:s',$r->commence_time) }}</td>
                                        <td class="align-center">{{$r->home_team}}</td>
                                        <td class="align-center">{{$r->away_team}}</td>
                                        <td class="align-center">{{$r->site_nice}}</td>
                                        <td class="align-center">{{$r->spreads_odds1}}<br/>{{$r->spreads_odds1}}</td>
                                        <td class="align-center">{{$r->spreads_points1}}</td>
                                        <td class="align-center">{{$r->spreads_points2}}</td>
                                    </tr>
									@endforeach
								@endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- #END# Task List -->
            </div>
			
        </section>
    </div>
	@include('layouts.FooterJS')
	@stack('scripts')
</body>
</html>
