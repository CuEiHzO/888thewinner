@extends('frontend.layout')
@section('content')
<style>
#apisoccer .hover:hover{
	cursor: pointer;
	background-color: #ffa;
}
</style>
<div class="container-fluid c88_content">
			<div class="row">
				<div class="col-12 col-md-3">
<!--
					<div class="input-group datepicker_table">
						<label>สร้างวันที่แข่ง</label>
						<div class="custom-file">
							<input id="datepicker" type="personal" class="form-control" />
						</div>
						<div class="input-group-append">
							<button class="send" type="button">สร้าง</button>
						</div>
					</div>
-->
				</div>
				<div class="col-12 col-md-5">
<!--					<button class="manage-table"><i class="fas fa-table"></i> จัดการตารางแข่งขัน</button>-->
				</div>
				<div class="col-12 col-md-4">
					<div class="input-group tableMatch row">
						<label>แสดงตารางแข่ง</label>
						<select class="month-select form-control" id="month">
							<option selected>{{ date('d F') }}</option>
						</select>
						<select class="year-select form-control" id="year">
							<option selected>{{ date('Y')}}</option>
						</select>
						<div class="input-group-append">
							<button class="send">แสดง</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="t88-table">
						<div class="table-responsive">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th align="center" rowspan="2" width="10%">เวลา</th>
										<th rowspan="2" width="30%">คู่แข่งขัน</th>
										<th colspan="3" width="30%">อัตราต่อรอง</th>
										<th colspan="3" width="30%">สูง-ต่ำ</th>
									</tr>
									<tr>
										<th>ราคา</th>
										<th>เจ้าบ้าน</th>
										<th>ทีมเยือน</th>
										<th>ราคา</th>
										<th>สูง</th>
										<th>ต่ำ</th>
									</tr>
								</thead>
								<tbody id="apisoccer">
@php 
$league_id = '';
@endphp
@if($sRow)
	@foreach( $sRow AS $Match )
		@if($Match->league_id != $league_id)
			<tr>
				<td colspan="8" class="tournament">{{ $Match->league_name }}</td>
			</tr>
		@php 
		$league_id  = $Match->league_id;
		@endphp
		@endif
		<tr data-matchID="{{ $Match->id }}" data-home="{{$Match->matche_home}}" data-away="{{$Match->matche_away}}" >
			<td>{{ date('H:i',strtotime($Match->matche_start)-3600)}}</td>
			<td><span class="bet">{{$Match->matche_home}}</span><br><span>{{$Match->matche_away}}</span> </td>
			<td>{{ isset($Match->hdp_f)?$Match->hdp_f:'-' }}</td>
			<td class="hover" data-type="home">{{ isset($Match->hdp_f_a)?$Match->hdp_f_a:'-' }}</td>
			<td class="hover" data-type="away">{{ isset($Match->hdp_f_b)?$Match->hdp_f_b:'-' }}</td>
			<td>{{ isset($Match->ou_f)?$Match->ou_f:'-' }}</td>
			<td class="hover" data-type="over">{{ isset($Match->ou_f_a)?$Match->ou_f_a:'-' }}</td>
			<td class="hover" data-type="under">{{ isset($Match->ou_f_b)?$Match->ou_f_b:'-' }}</td>
		</tr>			
	@endforeach
@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		
<style type="text/css">
table.timecard {
	border-collapse: collapse;
	border: 1px solid #fff; /*for older IE*/
	border-style: hidden;
	width: 350px;
}

table.timecard thead th {
	padding: 8px;
	background-color: #edcd00;
	font-size: large;
}

table.timecard th, table.timecard td {
	padding: 3px 5px;
	border-width: 1px;
	border-style: solid;
	border-color: #f79646 #ccc;
}

table.timecard td {
	font-size: 13px;
}


table.timecard tbody th {
	text-align: left;
	font-weight: normal;
}


table.timecard tr.even {
	background-color: #fde9d9;
}
</style>



<div style="position: fixed;left: 0;bottom: 0;padding:0px;margin-left: -350px;" id="divBet">
<div class="open_box"><p>รายการใบพนัน <i class="far fa-futbol"></i></p></div>
<div class="open_body" style="background-color: white;min-height:300px;width: 350px;">
	<table class="timecard">
		<tbody id="matchBet"></tbody>
		</tfoot>
			<tr>
				<td>
					<span id="Total">รวมเงินเดิมพัน</span><br><br>
					<button type="button" style="padding: 6px 20px;border-radius: .25rem;background: #edcd00;float: right;" id="BetSubmit">กดเพื่อบันทึกข้อมูล!</button>
				</td>
			</tr>
		</tfoot>
	</table>
</div>
</div>
@endsection




@push('scripts')
<script>
var iNum 	= 0;
var iTotal 	= 0;
var score 	= [];

$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
$(function() {
	$( '#apisoccer' ).on( "click", '.hover', function() {
		if( $(this).html() == '-' ) return; 
		var self 	= $(this);
		var id 		= self.parent().data('matchid');
		if( score.includes(id) ){
			return alert('ไม่สามารถแทงคู่เดิมได้');
		}else{
			score.push(id);
		}

	
		if( !$('.open_box').parent().hasClass("box-left") ){
			$('.open_box').parent().toggleClass("box-left");
		}
		
		if( self.data('type') == 'home' )	var txt = 'อัตราต่อรอง: '+self.parent().find('td:eq(2)').html()+'  เจ้าบ้าน @ '+self.parent().find('td:eq(3)').html();
		if( self.data('type') == 'away' )	var txt = 'อัตราต่อรอง: '+self.parent().find('td:eq(2)').html()+'  ทีมเยือน @ '+self.parent().find('td:eq(4)').html();
		if( self.data('type') == 'over' )  	var txt = 'สูงกว่า ต่ำกว่า:'+self.parent().find('td:eq(5)').html()+' สูงกว่า @ '+self.parent().find('td:eq(6)').html();
		if( self.data('type') == 'under' )	var txt = 'สูงกว่า ต่ำกว่า:'+self.parent().find('td:eq(5)').html()+' ต่ำกว่า @ '+self.parent().find('td:eq(7)').html();
		
		var trclass = $( '#matchBet tr:last' ).hasClass('odd')?'even':'odd';
		
		$( '#matchBet' ).append( ''
			+'<tr class="'+trclass+'">'
			+'	<td>'
			+'		<span>'+self.parent().find('td:eq(1) span:eq(0)').html()+' กับ '+self.parent().find('td:eq(1) span:eq(1)').html()+' </span><img src="888thewinner/images/delete.png" style="width: 16px;" class="cDelete" data-id="'+id+'"><br/>'
			+'		<span style="font-weight: lighter;">'+txt+'</span><br/>'
			+'		<span style="font-weight: lighter;">เงินเดิมพัน : '
			+'			<input type="number" value="10" style="height: 18px;text-align: center;width:80px;padding: 1px 0 0 0;font-size: 11px;" class="inputBet">'
			+'		</span>'
			+'	</td>'
			+'</tr>');
			
		$('#matchBet tr:last-child').data({id:id, type:self.data('type') });
		celBet();
		//console.log($('#matchBet tr:last-child').data());		
	});

	$( '#matchBet' ).on( "click", '.cDelete', function() {
		for( var i = 0; i < score.length; i++){ 
		   if ( score[i] === $(this).data('id')) {
			 score.splice(i, 1); 
			 i--;
		   }
		}
		$(this).parent().parent().remove();
		celBet();
	});
	
	$('#matchBet').on('keyup', '.inputBet', function(ev){
		celBet();
	});
	celBet=function(){
		if( $( '.inputBet' ).length ){
			iNum 	= 0;
			iTotal 	= 0;
			$( '.inputBet' ).each(function( index ) {
				var i = parseFloat($(this).val());
				if( isNaN(i) ) i=0;
				iTotal = i+iTotal;
				iNum++;
			});
			$('#Total').html('รวมเงินเดิมพัน '+iTotal+'บาท รวม '+iNum+' พนัน');
		}else{
			$('.open_box').parent().toggleClass("box-left");
		}
	}
	

	$( '.timecard' ).on( "click", '#BetSubmit', function() {
		if (confirm("ยืนยันรายการใบพนัน?!")) {
			var myData = {};
			myData['myBet'] = {};
			$( '#matchBet tr' ).each(function( index ) {
				myData['myBet'][index] = {id:$(this).data('id'), type:$(this).data('type'), amount:$(this).find('input').val() };		
			});
				
			console.log(myData);	
			
			$.ajax({
				type: 'POST',
				dataType: 'JSON',
				url: '{{ action("HomeController@betMatche") }}',
				data: myData,
				success: function(result) {
					if( result.status == 'success' ){
						score = [];
						$('.open_box').parent().toggleClass("box-left");
						$('#matchBet tr').remove();
						window.open('user/bet/'+result.code,'popUpWindow','height=450,width=350,left=100,top=100,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no, status=yes');
					}else{
						alert('พบข้อผิดพลาด ลองทำรายการอีกครั้ง');
						location.reload();
					}
				},
				error: function(xhr, error){
					alert('พบข้อผิดพลาด ลองทำรายการอีกครั้ง');
					location.reload();
				}
			});
		}
	});
	
	
	$('.open_box').click(function() {
		$(this).parent().toggleClass("box-left");
	});
});
</script>
@endpush
