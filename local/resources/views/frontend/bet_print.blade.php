<html>
<head>
	<title></title>
	<style>
table {
  border-collapse: collapse;
  font-size: 11px;
}

table, th, td {
	border: 1px solid black;
	text-align: center;
	padding:5px;
}

	</style>
</head>

<body>

	<div style="text-align: center;font-weight: bold;font-size: 24px;">888TheWinner</div>
	<div style="text-align: center;font-size: 12px;padding: 8px;">วันที่ {{ date('d-m-Y H:i:s', strtotime($sRow->created_at)) }}</div>
	<div style="text-align: center;font-size: 12px;padding:0 0 10px 0;">เลขที่บิล {{$sRow->code}}</div>
	<table style="width: 100%;">
		<thead>
			<tr>
				<th colspan="2">รายระเอียดการแทง</th>
				<th>ถูกต่อ</th>
				<th>ราคา</th>
				<th>แทง</th>
			</tr>
		</thead>
		<tbody>
		@if($sRow->list)
			@foreach($sRow->list AS $r)
		@php
		if( $r->type=='home' ) $type ='เจ้าบ้าน';
		if( $r->type=='away' ) $type ='ทีมเยือน';
		if( $r->type=='over' ) $type ='สูงกว่า';
		if( $r->type=='under' ) $type ='ต่ำกว่า';
		@endphp
		
			<tr>
				<td>{{ $type }}</td>
				<td>{{$r->match->matche_home}} - {{$r->match->matche_away}}</td>
				<td>{{$r->bet1}}</td>
				<td>{{$r->bet2}}</td>
				<td>{{$r->amount}}</td>
			</tr>
			@endforeach
		@endif
		</tbody>
		<tfoot>
			<tr>
				<td colspan="5">รวมเงินเดิมพัน {{$sRow->amount}}บาท รวม {{$sRow->list->count()}} พนัน</td>
			</tr>
		</tfoot>
	</table>

</body>
</html>