<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('888thewinner/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('888thewinner/assets/css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('888thewinner/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('888thewinner/assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('888thewinner/assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('888thewinner/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
	
	<link href="{{asset('888thewinner/assets/css/lobibox.min.css')}}" rel="stylesheet">
	<link href="{{asset('888thewinner/assets/css/animate.css')}}" rel="stylesheet">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->


	<!-- Theme JS files -->
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/core/libraries/jquery_ui/widgets.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/core/libraries/jquery_ui/effects.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/extensions/mousewheel.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/core/libraries/jquery_ui/globalize/globalize.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/core/libraries/jquery_ui/globalize/cultures/globalize.culture.de-DE.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/core/libraries/jquery_ui/globalize/cultures/globalize.culture.ja-JP.js')}}"></script>
	
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/ui/nicescroll.min.js')}}"></script>

	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/pages/form_layouts.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/core/app.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/pages/layout_fixed_custom.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/pages/jqueryui_forms.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/pages/datatables_basic.js')}}"></script>
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
	
	<script type="text/javascript" src="{{asset('888thewinner/assets/js/pages/form_checkboxes_radios.js')}}"></script>

	<script type="text/javascript" src="{{asset('888thewinner/assets/js/pages/components_modals.js')}}"></script>

	<!-- Lobibox nofitication -->
	<script src="{{asset('888thewinner/assets/js/lobibox.js')}}"></script>
	<script src="{{asset('888thewinner/assets/js/demo.js')}}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ asset('888thewinner/assets/js/bootbox.min.js') }}"></script>

	
<!-- /theme JS files -->