<!DOCTYPE html>
<html lang="en">
<head>
	<title>888thewinner</title>
	<meta charset="utf-8">
	<base href="{{ asset('') }}">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- open if have favicon
	<link type="image/ico" rel="shortcut icon" href="888thewinner/images/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="888thewinner/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="888thewinner/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="888thewinner/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="888thewinner/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="888thewinner/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="888thewinner/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="888thewinner/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="888thewinner/apple-touch-icon-152x152.png">
	-->
	<link type="text/css" rel="stylesheet" href="888thewinner/css/reset.css" media="screen,projection" />
	<link type="text/css" rel="stylesheet" href="888thewinner/css/print.css" media="print" />

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,500,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kanit:300,500,700" rel="stylesheet">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<!-- Bootstrap -->
	<link href="888thewinner/css/bootstrap.min.css" rel="stylesheet">

	<link type="text/css" rel="stylesheet" href="888thewinner/css/layout.css" media="screen,projection" />



	<script type="text/javascript" src="888thewinner/js/jquery-3.3.1.slim.min.js"></script>
	<script type="text/javascript" src="888thewinner/js/jquery.min.js"></script>
	<script src="888thewinner/js/popper.min.js"></script>
	<script src="888thewinner/js/bootstrap.min.js"></script>

	<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
	<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>

<body>
	@include('frontend.inc_head')
	<!-- content here-->
	<div class="wrapper">
	    @yield('content')
	</div>
	<!-- end content here -->
	@include('frontend.inc_footer')
	@stack('scripts')
</body>

</html>
