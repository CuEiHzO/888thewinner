<header>
	<section class="navigation">
		<div class="top-nav">
			<div class="container-fluid">
				<div class="row">
					<div class="col"></div>
					<div class="col-12 col-md-6">
						<ul class="login-top">
							<li><a href="#"><i class="fas fa-user"></i> เข้าสู่ระบบ</a></li>
							<li><a href="#" style="border: none;">ออกจากระบบ</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="nav-container">
				<div class="brand">
					<a href="index.php"><img src="888thewinner/images/logo.png"></a>
				</div>
				<nav>
					<div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
					<ul class="nav-list">
						<li>
							<a href="#" class="table">ตารางแข่งขัน</a>
						</li>
						<li>
							<a href="#" class="info">ข้อมูลทีม</a>
						</li>
						<li>
							<a href="#" class="info-user">ข้อมูลผู้ใช้ & การตั่งค่า</a>
						</li>
						<li>
							<a href="#" class="chart">สถิติการแทง</a>
						</li>				 	          		 	         
						<li>
							<a href="#" class="reoport">รายงานภาพรวม</a>
						</li>
						<li>
							<a href="#" class="store-report">รายงานเก็บ</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</section>
</header>


