<!-- Main navbar -->
<div class="navbar navbar-inverse navbar-fixed-top">
<div class="navbar-header">
<a class="navbar-brand" href="{{url('/')}}"><font size="4">Football</font></a>

<ul class="nav navbar-nav visible-xs-block">
	<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
	<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
</ul>
</div>

<div class="navbar-collapse collapse" id="navbar-mobile">
<ul class="nav navbar-nav">
	<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
</ul>

<p class="navbar-text"><span class="label bg-success">ออนไลน์</span></p>

<ul class="nav navbar-nav navbar-right">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-bubbles4"></i>
			<span class="visible-xs-inline-block position-right">Messages</span>
			<span class="badge bg-warning-400">2</span>
		</a>
		
		<div class="dropdown-menu dropdown-content width-350">
			<div class="dropdown-content-heading">
				Messages
				<ul class="icons-list">
					<li><a href="#"><i class="icon-compose"></i></a></li>
				</ul>
			</div>

			<ul class="media-list dropdown-content-body">
				<li class="media">
					<div class="media-left">
						<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
						<span class="badge bg-danger-400 media-badge">4</span>
					</div>

					<div class="media-body">
						<a href="#" class="media-heading">
							<span class="text-semibold">ผู้ทดสอบระบบ</span>
							<span class="media-annotation pull-right">04:58</span>
						</a>

						<span class="text-muted">who knows, maybe that would be the best thing for me...</span>
					</div>
				</li>

				<li class="media">
					<div class="media-left">
						<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
						<span class="badge bg-danger-400 media-badge">4</span>
					</div>

					<div class="media-body">
						<a href="#" class="media-heading">
							<span class="text-semibold">Margo Baker</span>
							<span class="media-annotation pull-right">12:16</span>
						</a>

						<span class="text-muted">That was something he was unable to do because...</span>
					</div>
				</li>

			</ul>

			<div class="dropdown-content-footer">
				<a href="#" data-popup="tooltip" title="" data-original-title="All messages"><i class="icon-menu display-block"></i></a>
			</div>
		</div>
	</li>
					
	<li class="dropdown dropdown-user">
		<a class="dropdown-toggle" data-toggle="dropdown">
			<img src="{{asset('assets/images/placeholder.jpg')}}" alt="">
			<span><?php echo Auth::user()->name;?></span>
			<i class="caret"></i>
		</a>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="#"><i class="icon-cog5"></i> ตั้งค่าบัญชีส่วนตัว</a></li>
			<li><a href="{{url('logout')}}"><i class="icon-switch2"></i> ลงชื่อออก</a></li>
		</ul>
	</li>
</ul>
</div>
</div>
<!-- /main navbar -->

<!-- Page container -->
<div class="page-container">

<!-- Page content -->
<div class="page-content">

<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-fixed">
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user">
			<div class="category-content">
				<div class="media">
					<a href="#" class="media-left"><img src="{{asset('assets/images/placeholder.jpg')}}" class="img-circle img-sm" alt=""></a>
					<div class="media-body">
						<span class="media-heading text-semibold"><?php echo Auth::user()->name;?></span>
						<div class="text-size-mini text-muted">
							<i class="icon-pin text-size-small"></i> &nbsp;
						</div>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list">
							<li>
								<a href="#"><i class="icon-cog3"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /user menu -->

		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">
					<!-- Main -->
					<li class="navigation-header"><span></span> <i class="icon-menu" title="Main pages"></i></li>
					<li><a href="{{url('dashboard')}}"><i class="icon-home"></i> <span>หน้าแรก</span></a></li>
					<li><a href="{{url('member')}}"><i class="icon-user-lock"></i> <span>จัดการสมาชิก</span></a></li>
					<li><a href="{{url('league')}}"><i class="icon-list"></i> <span>ลีค</span></a></li>
					<li><a href="{{url('match')}}"><i class="fa fa-futbol-o"></i> <span>แมตช์</span></a></li>
					
					
					
					
				</ul>
			</div>
		</div>
		<!-- /main navigation -->

	</div>
</div>
<!-- /main sidebar -->
