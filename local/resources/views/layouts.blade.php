<!DOCTYPE html>
<html lang="en">
<head>
@include('layouts.Header')
</head>
<body class="">
    <div class="all-content-wrapper">
        <!-- Top Bar -->
		@include('layouts.TopBar')
        <!-- #END# Top Bar -->
		
        <!-- Left Menu -->
		@include('layouts.Sidebar')
        <!-- #END# Left Menu -->
       
        <!-- Section Content -->
		@yield('content')
        <!-- #END# Section Content -->
    </div>
	@include('layouts.FooterJS')
	@stack('scripts')
</body>
</html>
